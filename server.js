require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const APP_PORT = process.env.APP_PORT || 3000;
const path = require('path');
const db = require('./db');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/src/public'));
app.set('views', path.join(__dirname, '/src/views'));
app.set('view engine', 'ejs');

db.connect(process.env.DB_URL, function (err) {
   if (err) {
       return console.log(err);
   }
    require('./src/routes')(app);
   app.listen(APP_PORT, function () {
       console.log("Server started on port " + APP_PORT);
   })
});
let db = require('../../db');
let ObjectID = require('mongodb').ObjectID;

exports.all = function (cb) {
    db.get().collection('notes').find().toArray(function (err, docs) {
        cb(err, docs);
    })
};

exports.findById = function (id, cb) {
    let objectId = {'_id': new ObjectID(id)};
    db.get().collection('notes').findOne( objectId, (err, item) => {
        cb(err, item);
    });
};

exports.remove = function (id, cb) {
    let objectId = {'_id': new ObjectID(id)};
    db.get().collection('notes').remove( objectId, (err) => {
        cb(err);
    });
};

exports.findOneAndReplace = function (id , objectData, cb) {
  let objectId =  {'_id': new ObjectID(id)};
  db.get().collection('notes').findOneAndReplace(objectId, objectData, (err, item) => {
      cb(err, item);
  });
};

exports.create = function (objectData, cb) {
  db.get().collection('notes').insertOne(objectData, (err, result) => {
      cb(err, result['ops']);
  });
};
const noteRoutes = require('./note_routes');
const baseRoutes = require('./base_routes');
module.exports = function (app) {
    noteRoutes(app);
    baseRoutes(app);
};
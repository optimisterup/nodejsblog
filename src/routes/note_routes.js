let artistsController = require('../controllers/notes');

module.exports = function (app) {

    app.get('/notes', artistsController.all);

    app.get('/note/:id', artistsController.findById);

    app.delete('/note/delete/:id', artistsController.delete);

    app.put('/note/update/:id', artistsController.findOneAndReplace);

    app.post('/note/add', artistsController.create);
};

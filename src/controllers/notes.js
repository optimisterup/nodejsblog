let Notes = require('../models/note');

function checkError(err, res) {
    if (err) {
        console.log(err);
        return res.status(500).send({'error':'An error has occurred'});
    }
}

exports.all = function (req, res) {
    Notes.all(function (err, items) {
        checkError(err, res);
        res.send(items);
    })
};

exports.findById = function (req, res) {
    Notes.findById( req.params.id, function (err, item){
        checkError(err, res);
        res.send(item);
    });
};

exports.delete = function (req, res) {
    let id =  req.params.id;
    Notes.remove(id, function (err) {
        checkError(err, res);
        res.status(200).send('{"status": "success" }');
    });
};

exports.findOneAndReplace = function (req, res) {
    let id = req.params.id;
    let noteData = {name: req.body.name, description: req.body.description};
    Notes.findOneAndReplace(id, noteData, function (err) {
        checkError(err);
        res.status(200).send(noteData);
    });
};

exports.create = function (req, res) {
    let noteData = {name: req.body.name, description: req.body.description};
    Notes.create(noteData, function (err, item) {
        checkError(err);
        res.status(200).send(item);
    })
};